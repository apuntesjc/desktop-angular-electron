# Base proyecto angular y electron

Proyecto de Angular al cual se le instaló Electron para desarrollar una aplicación de escritorio

## Instalar electron

npm install -D electron@latest

## Generación archivo para configurar electron

- Primero crear un archivo de nombre "main.js" en la raíz del proyecto
- Revisar documento de este proyecto para saber que estructura debe tener y que datos base se deben ingresar

## Configurar angular.json

- En sección de build, cambiar la salida y dejar solo "dist"

## Levantar electron con angular

- Configurar comando en package.json "electron" (Revisar archivo)

## Generar build

- Revisar package.json en donde se agregaron comandos para build de aplicación
- Revisar que el comando tenga todo lo que necesario (Puede modificar nombre, icono, entre otros)
